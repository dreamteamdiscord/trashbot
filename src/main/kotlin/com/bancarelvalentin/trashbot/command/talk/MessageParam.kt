package com.bancarelvalentin.trashbot.command.talk

import com.bancarelvalentin.ezbot.process.command.param.StringCommandParam

class MessageParam : StringCommandParam() {
    override val rawName = "Message"
    override val rawDesc = "Message to send"
}