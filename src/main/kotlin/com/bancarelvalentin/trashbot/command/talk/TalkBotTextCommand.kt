package com.bancarelvalentin.trashbot.command.talk

import com.bancarelvalentin.ezbot.process.command.Command
import com.bancarelvalentin.ezbot.process.command.param.CommandParam
import com.bancarelvalentin.ezbot.process.command.request.CommandRequest
import com.bancarelvalentin.ezbot.process.command.response.CommandResponse
import java.util.function.BiConsumer


class TalkBotTextCommand : Command() {

    override val whitelistChannelIds = arrayOf(773624720444227614)
    override val whitelistRoleIds = arrayOf(770732667137425448)
    override val patterns = arrayOf("talk", "t")
    override val paramsClasses: Array<Class<out CommandParam<out Any?>>> =
        arrayOf(ChannelParam::class.java, MessageParam::class.java)

    override val rawName = "Bot Talking"
    override val rawDesc = "Send a message in achoosen channel as the bot"
    override val sample = "#general Coucou mes p'tits loups !"

    override val logic = BiConsumer { request: CommandRequest, _: CommandResponse ->
        val channel = request.getParamChannel(0)!!
        val msg = request.joinParams(1)!!
        channel.sendMessage(msg).complete()
    }

}
